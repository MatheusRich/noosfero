# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2015-02-24 20:46+0200\n"
"Last-Translator: Gonzalo Exequiel Pedone <hipersayan.x@gmail.com>\n"
"Language-Team: Spanish <https://hosted.weblate.org/projects/noosfero/plugin-"
"comment-classification/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.3-dev\n"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:16
msgid "Label created"
msgstr "Etiqueta creada"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:19
msgid "Label could not be created"
msgstr "La etiqueta no pudo ser creada"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:31
msgid "Label updated"
msgstr "Etiqueta actualizada"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:34
msgid "Failed to edit label"
msgstr "Fallo al editar la etiqueta"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:45
msgid "Label removed"
msgstr "Etiqueta eliminada"

#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:47
#: ../controllers/admin/comment_classification_plugin_labels_controller.rb:50
msgid "Label could not be removed"
msgstr "No se puede eliminar la etiqueta"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:15
msgid "Status created"
msgstr "Estado creado"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:18
msgid "Status could not be created"
msgstr "No se puede crear el estado"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:29
msgid "Status updated"
msgstr "Estado actualizado"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:32
msgid "Failed to edit status"
msgstr "Fallo al editar el estado"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:43
msgid "Status removed"
msgstr "Estado eliminado"

#: ../controllers/admin/comment_classification_plugin_status_controller.rb:45
#: ../controllers/admin/comment_classification_plugin_status_controller.rb:48
msgid "Status could not be removed"
msgstr "No se puede eliminar el estado"

#: ../lib/comment_classification_plugin.rb:11
msgid "A plugin that allow classification of comments."
msgstr "Un plugin que permite clasificar comentarios."

#: ../views/comment/comment_extra.html.erb:12
#: ../views/comment_classification_plugin_myprofile/_status_form.html.erb:7
#: ../views/comment_classification_plugin_status/index.html.erb:9
msgid "Status"
msgstr "Estado"

#: ../views/comment/comments_labels_select.html.erb:3
#, fuzzy
msgid "Label..."
msgstr "Etiqueta"

#: ../views/comment_classification_plugin_admin/index.html.erb:1
msgid "Comments classification options"
msgstr "Opciones de clasificación de comentarios"

#: ../views/comment_classification_plugin_admin/index.html.erb:4
msgid "Manage Labels"
msgstr "Gestionar Etiquetas"

#: ../views/comment_classification_plugin_admin/index.html.erb:5
msgid "Manage Status"
msgstr "Gestionar Estados"

#: ../views/comment_classification_plugin_labels/_form.html.erb:6
#: ../views/comment_classification_plugin_status/_form.html.erb:6
msgid "Name"
msgstr "Nombre"

#: ../views/comment_classification_plugin_labels/_form.html.erb:7
#: ../views/comment_classification_plugin_labels/index.html.erb:10
msgid "Color"
msgstr "Color"

#: ../views/comment_classification_plugin_labels/_form.html.erb:8
msgid "Enable this label?"
msgstr "¿Habilitar esta etiqueta?"

#: ../views/comment_classification_plugin_labels/create.html.erb:1
#: ../views/comment_classification_plugin_labels/index.html.erb:29
msgid "Add a new label"
msgstr "Agregar una nueva etiqueta"

#: ../views/comment_classification_plugin_labels/edit.html.erb:1
msgid "Editing label %s"
msgstr "Editar etiqueta %s"

# pendiente
#: ../views/comment_classification_plugin_labels/index.html.erb:1
msgid "Manage comments labels"
msgstr "Gestionar la etiqueta de los comentarios"

# Le ponemos "no registrado" o "sin registrar"? O no especificado?
#: ../views/comment_classification_plugin_labels/index.html.erb:5
msgid "(no label registered yet)"
msgstr "(todavía no se ha registrado una etiqueta)"

#: ../views/comment_classification_plugin_labels/index.html.erb:9
msgid "Label"
msgstr "Etiqueta"

#: ../views/comment_classification_plugin_labels/index.html.erb:11
#: ../views/comment_classification_plugin_status/index.html.erb:10
msgid "Enabled"
msgstr "Habilitado"

#: ../views/comment_classification_plugin_labels/index.html.erb:12
#: ../views/comment_classification_plugin_status/index.html.erb:12
msgid "Actions"
msgstr "Acciones"

#: ../views/comment_classification_plugin_labels/index.html.erb:21
msgid "Are you sure you want to  remove this label?"
msgstr "¿Estás seguro de que deseas eliminar esta etiqueta?"

#: ../views/comment_classification_plugin_myprofile/_status_form.html.erb:8
msgid "Reason:"
msgstr "Razón:"

#: ../views/comment_classification_plugin_myprofile/add_status.html.erb:1
msgid "Status for comment"
msgstr "Estado del comentario"

#: ../views/comment_classification_plugin_myprofile/add_status.html.erb:5
msgid "Comment Informations"
msgstr ""

#: ../views/comment_classification_plugin_myprofile/add_status.html.erb:18
#, fuzzy
msgid "added the status"
msgstr "Agregar nuevo estado"

#: ../views/comment_classification_plugin_myprofile/add_status.html.erb:23
#, fuzzy
msgid "Reason"
msgstr "Razón:"

#: ../views/comment_classification_plugin_myprofile/add_status.html.erb:31
#: ../views/comment_classification_plugin_status/create.html.erb:1
#: ../views/comment_classification_plugin_status/index.html.erb:29
msgid "Add a new status"
msgstr "Agregar nuevo estado"

#: ../views/comment_classification_plugin_myprofile/index.html.erb:1
msgid "Manage comment classification"
msgstr "Gestionar la clasificación de comentarios"

#: ../views/comment_classification_plugin_status/_form.html.erb:7
msgid "Enable this status?"
msgstr "¿Habilitar este estado?"

#: ../views/comment_classification_plugin_status/edit.html.erb:1
msgid "Editing status %s"
msgstr "Editando el estado %s"

#: ../views/comment_classification_plugin_status/index.html.erb:1
msgid "Manage comments status"
msgstr "Gestionar el estado de los comentarios"

# Le ponemos "no registrado" o "sin registrar"? O no especificado?
#: ../views/comment_classification_plugin_status/index.html.erb:5
msgid "(no status registered yet)"
msgstr "(todavía no se ha registrado un estado)"

# habilitado o permitido? estaba o está?
#: ../views/comment_classification_plugin_status/index.html.erb:11
msgid "Reason enabled?"
msgstr "¿Razón habilitada?"

#: ../views/comment_classification_plugin_status/index.html.erb:21
msgid "Are you sure you want to remove this status?"
msgstr "¿Estás seguro de que deseas eliminar este estado?"

#~ msgid "[Select ...]"
#~ msgstr "[Seleccione ...]"

#~ msgid ""
#~ "<i>%{user}</i> added the status <i>%{status_name}</i> at <i>%{created_at}"
#~ "</i>."
#~ msgstr ""
#~ "<i>%{user}</i> ha agregado el estado <i>%{status_name}</i> en <i>"
#~ "%{created_at}</i>."

#~ msgid "<i>Reason:</i> %s"
#~ msgstr "<i>Razón:</i> %s"
